# websearch project

## technology stack

+ **java version**: 1.8+
+ **build tool**: apache maven
+ **framework**: spring
+ **http listener**: renetty ([renetty](https://github.com/nimabt/renetty) is a small project of mine which simplifies the usage of the "Netty" project with providing a set of handy annotations)


### how to compile
``$ mvn clean install -P[maven profile]``
### maven profiles
+ **development**: development profile*(active-by default)*
+ **live**: live profile
 
## how to build
after the compilation process, please run the deploy.sh file inorder to get the binary project.
also, there's init.d scripts in the etc/init.d folder to help running the project as *nix daemon
 
`` $ bash deploy.sh ``

the default directory for the binary output is: */tmp/websearch-$PROJECT_VERSION* (*/tmp/websearch-0.1.0*)

after that, create log dir with the help of following command:

`` $ mkdir /var/log/websearch ``

## how to run
 + **compile**: compile the project
 + **build**: build the binary project
 + **run**: go to ``$PROJECT_BUILD_HOME/websearch-service/bin`` and execute the following command:
 ``$ bash websearch-service.sh ``

the project will listen to http requests on **port: 5000**
 

## api info

### search api
#### request
+ **address** : /api/search?keyword=SEARCH_KEY
+ **method** : GET

#### response
+ **response-type**: application/json
* **data structure**: SearchResponse

*entity: SearchResponse*

+ **status** (int) : *result status: 0 represents ok, otherwise there's a error- check response values*
+ **results** (SearchResult[])
    + **title** (String)
    + **searchType** (enum:ALBUM,BOOK) : *determines whether it's a book or an album*
    + **authors** (String)
    
    
#### sample

``` 
http://PROJECT_HOST:5000/api/search?keyword=pink+floyd
```

```
{
    "results":[{"title":"Another Brick In the Wall, Pt. 2","type":"ALBUM","authors":"Pink Floyd"},{"title":"Comfortably Numb","type":"ALBUM","authors":"Pink Floyd"},{"title":"Echoes","type":"BOOK","authors":"Glenn Povey"},{"title":"Guitar World Presents Pink Floyd","type":"BOOK","authors":"Alan Di PernaJeff KittsBrad Tolinski"},{"title":"Hey You","type":"ALBUM","authors":"Pink Floyd"},{"title":"Mind Over Matter","type":"BOOK","authors":"Peter Curzon"},{"title":"Money","type":"ALBUM","authors":"Pink Floyd"},{"title":"Pink Floyd and Philosophy","type":"BOOK","authors":"George A. Reisch"},{"title":"Pink Floyd: The Music and the Mystery","type":"BOOK","authors":"Andy Mabbett"},{"title":"Wish You Were Here","type":"ALBUM","authors":"Pink Floyd"}],
    "status":0
}
```


 
### service status api
#### request
+ **address** : /api/service-status
+ **method** : GET

#### response
+ **response-type** : application/json
+ **data structure** : ServiceStatusResponse

*entity: ServiceStatusResponse*

+ **status** (int) : *result status: 0 represents ok, otherwise there's a error- check response values*
+ **serviceStatus** (ServiceStatus)
    + **uptime** (long) : *service uptime in unix timestamp (milliseconds)*
    + **totalRequests** (long) : *the count of total requests received*
    + **successRequests** (long) : *the count of succeeded requests (requests with status:0)*
    + **averageResponseTime** (long) : *the response average time of the latest 10 api calls of the system*
    + **averageAlbumApiCallTime** (long) : *the response average time of the latest 10 api calls for itunes-album-search api*
    + **averageBookApiCallTime** (long) : *the response average time of the latest 10 api calls for google-book-search api*
    + **failedAlbumApiCalls** (long) : *the count of the failed 'itunes album' upstream api calls*
    + **failedBookApiCalls** (long) : *the count of the failed 'google book' upstream api calls*
    
 
##### sample

```
http://PROJECT_HOST:5000/api/service-status
```

```
{
    "serviceStatus":{"uptime":73420,"totalRequests":5,"successRequests":5,"averageResponseTime":1346,"averageAlbumApiCallTime":736,"averageBookApiCallTime":1137,"failedAlbumApiCalls":0,"failedBookApiCalls":0}
    ,"status":0
}
```

## response code values
+ **0**   : *ok*
+ **-1001** : *ERR: empty search keyword*
+ **-2001** : *ERR: failed to call upstream services* 
+ **-9001** : *ERR: internal error, please contact the support team*





