package com.kramphub.websearch.common.dao;

import com.kramphub.websearch.common.entity.itunes.ITunesAlbumSearchResponse;
import com.kramphub.websearch.common.exception.WebSearchException;

/**
 * @author: nima.abt
 * @since: 9/24/17
 */
public interface AlbumSearchDao {

    public ITunesAlbumSearchResponse search(String keyword, int maxResultCount) throws WebSearchException;

}
