package com.kramphub.websearch.common.entity.google;

import java.io.Serializable;
import java.util.Arrays;

/**
 * @author: nima.abt
 * @since: 9/25/17
 */
public class GoogleBookSearchResponse implements Serializable {


    private String kind;
    private long totalItems;
    private GoogleBookSearchResult[] items;


    public GoogleBookSearchResponse(){

    }


    public String getKind() {
        return kind;
    }

    public void setKind(final String kind) {
        this.kind = kind;
    }

    public long getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(final long totalItems) {
        this.totalItems = totalItems;
    }


    public GoogleBookSearchResult[] getItems() {
        return items;
    }

    public void setItems(final GoogleBookSearchResult[] items) {
        this.items = items;
    }


    @Override
    public String toString() {
        return "GoogleBookSearchResponse{" +
                "kind='" + kind + '\'' +
                ", totalItems=" + totalItems +
                ", items=" + Arrays.toString(items) +
                '}';
    }




}
