package com.kramphub.websearch.common.dao.http;

import com.google.gson.Gson;
import com.kramphub.websearch.common.dao.AlbumSearchDao;
import com.kramphub.websearch.common.entity.itunes.ITunesAlbumSearchResponse;
import com.kramphub.websearch.common.exception.WebSearchException;
import com.kramphub.websearch.common.system.ResponseCodes;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URLEncoder;

/**
 * @author: nima.abt
 * @since: 9/24/17
 */
public class ITunesHttpAlbumSearchDao implements AlbumSearchDao {

    private final String baseUrl;
    private final String DEFAULT_ENCODING = "utf-8";

    private final Logger logger = Logger.getLogger(ITunesHttpAlbumSearchDao.class);

    public ITunesHttpAlbumSearchDao(final String baseUrl){
        logger.info("init: ITunesHttpAlbumSearchDao ... ");
        this.baseUrl = baseUrl;
    }


    @Override
    public ITunesAlbumSearchResponse search(final String keyword, final int maxResultCount) throws WebSearchException {

        try{

            final String url = baseUrl + "?term=" + URLEncoder.encode(keyword,DEFAULT_ENCODING) + "&limit=" + maxResultCount;

            if(logger.isDebugEnabled()){
                logger.debug("prepare to call url: " + url);
            }

            final CloseableHttpClient httpclient = HttpClients.createDefault();
            final HttpGet httpGet = new HttpGet(url);
            final CloseableHttpResponse response = httpclient.execute(httpGet);

            final String body = IOUtils.toString(response.getEntity().getContent(),DEFAULT_ENCODING);
            if(logger.isDebugEnabled()){
                logger.debug("got response: " + body);
            }

            return new Gson().fromJson(body,ITunesAlbumSearchResponse.class);

        } catch (IOException e){
            throw new WebSearchException(ResponseCodes.ERR_UPSTREAM_CALL_FAILED,"IOException occurred while fetching data; reason: " + e.getMessage());
        } catch (Exception e){
            throw new WebSearchException(ResponseCodes.ERR_UPSTREAM_CALL_FAILED,"UnknownException occurred while fetching data; reason: " + e.getMessage());
        }

    }






}
