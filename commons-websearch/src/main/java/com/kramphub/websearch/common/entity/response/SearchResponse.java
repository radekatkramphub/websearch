package com.kramphub.websearch.common.entity.response;

import com.kramphub.websearch.common.entity.SearchResult;

import java.io.Serializable;
import java.util.Arrays;

/**
 * @author: nima.abt
 * @since: 9/24/17
 */
public class SearchResponse extends AbstractResponse implements Serializable {

    private SearchResult[] results;


    public SearchResponse(){

    }

    public SearchResponse(final int status, final SearchResult[] results){
        super(status);
        this.results = results;
    }


    public SearchResult[] getResults() {
        return results;
    }

    public void setResults(final SearchResult[] results) {
        this.results = results;
    }


    @Override
    public String toString() {
        return "SearchResponse{" +
                super.toString() +
                ", results=" + Arrays.toString(results) +
                '}';
    }


}
