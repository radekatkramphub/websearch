package com.kramphub.websearch.common.util;

import com.kramphub.websearch.common.entity.google.GoogleBookSearchResponse;
import com.kramphub.websearch.common.entity.google.GoogleBookSearchResult;
import com.kramphub.websearch.common.entity.itunes.ITunesAlbumSearchResponse;
import com.kramphub.websearch.common.entity.itunes.ITunesAlbumSearchResult;
import com.kramphub.websearch.common.entity.SearchResult;
import com.kramphub.websearch.common.entity.response.SearchType;
import org.apache.commons.lang.StringUtils;

import java.util.*;

/**
 * @author: nima.abt
 * @since: 9/25/17
 */
public class SearchUtil {

    public static SearchResult toSearchResult(final ITunesAlbumSearchResult iTunesAlbumSearchResult){

        if(iTunesAlbumSearchResult==null) return null;

        return new SearchResult(iTunesAlbumSearchResult.getTrackName(), SearchType.ALBUM, iTunesAlbumSearchResult.getArtistName());

    }


    public static SearchResult toSearchResult(final GoogleBookSearchResult googleBookSearchResult){

        if(googleBookSearchResult==null) return null;

        final String authors = StringUtils.join(googleBookSearchResult.getVolumeInfo().getAuthors());
        return new SearchResult(googleBookSearchResult.getVolumeInfo().getTitle(),SearchType.BOOK,authors);

    }


    public static List<SearchResult> toSearchResult(final ITunesAlbumSearchResponse response){

        final List<SearchResult> result = new LinkedList<>();
        if(response==null || response.getResults()==null || response.getResults().length==0) return result;

        for(final ITunesAlbumSearchResult item : response.getResults()){
            final SearchResult searchResult = toSearchResult(item);
            if(searchResult!=null)
                result.add(searchResult);
        }

        return result;

    }



    public static List<SearchResult> toSearchResult(final GoogleBookSearchResponse response){

        final List<SearchResult> result = new LinkedList<>();
        if(response==null || response.getItems()==null || response.getItems().length==0) return result;

        for(final GoogleBookSearchResult item : response.getItems()){
            final SearchResult searchResult = toSearchResult(item);
            if(searchResult!=null)
                result.add(searchResult);
        }

        return result;

    }




}
