package com.kramphub.websearch.common.entity.itunes;

import java.io.Serializable;
import java.util.Arrays;

/**
 * @author: nima.abt
 * @since: 9/25/17
 */
public class ITunesAlbumSearchResponse implements Serializable {

    private long resultCount;
    private ITunesAlbumSearchResult[] results;


    public ITunesAlbumSearchResponse(){

    }


    public long getResultCount() {
        return resultCount;
    }

    public void setResultCount(final long resultCount) {
        this.resultCount = resultCount;
    }

    public ITunesAlbumSearchResult[] getResults() {
        return results;
    }

    public void setResults(final ITunesAlbumSearchResult[] results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "ITunesAlbumSearchResponse{" +
                "resultCount=" + resultCount +
                ", results=" + Arrays.toString(results) +
                '}';
    }


}
