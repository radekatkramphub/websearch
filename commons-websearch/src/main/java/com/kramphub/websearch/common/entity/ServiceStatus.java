package com.kramphub.websearch.common.entity;

import java.io.Serializable;

/**
 * @author: nima.abt
 * @since: 9/27/17
 */
public class ServiceStatus implements Serializable {

    private long uptime;

    private long totalRequests;
    private long successRequests;

    private long averageResponseTime;

    private long averageAlbumApiCallTime;
    private long averageBookApiCallTime;

    private long failedAlbumApiCalls;
    private long failedBookApiCalls;


    public ServiceStatus() {

    }


    public ServiceStatus(
        final long uptime,
        final long totalRequests,
        final long successRequests,
        final long averageResponseTime,
        final long averageAlbumApiCallTime,
        final long averageBookApiCallTime,
        final long failedAlbumApiCalls,
        final long failedBookApiCalls
    ) {
        this.uptime = uptime;
        this.totalRequests = totalRequests;
        this.successRequests = successRequests;
        this.averageResponseTime = averageResponseTime;
        this.averageAlbumApiCallTime = averageAlbumApiCallTime;
        this.averageBookApiCallTime = averageBookApiCallTime;
        this.failedAlbumApiCalls = failedAlbumApiCalls;
        this.failedBookApiCalls = failedBookApiCalls;
    }


    public long getUptime() {
        return uptime;
    }

    public void setUptime(final long uptime) {
        this.uptime = uptime;
    }


    public long getAverageResponseTime() {
        return averageResponseTime;
    }

    public void setAverageResponseTime(final long averageResponseTime) {
        this.averageResponseTime = averageResponseTime;
    }

    public long getAverageAlbumApiCallTime() {
        return averageAlbumApiCallTime;
    }

    public void setAverageAlbumApiCallTime(final long averageAlbumApiCallTime) {
        this.averageAlbumApiCallTime = averageAlbumApiCallTime;
    }

    public long getAverageBookApiCallTime() {
        return averageBookApiCallTime;
    }

    public void setAverageBookApiCallTime(final long averageBookApiCallTime) {
        this.averageBookApiCallTime = averageBookApiCallTime;
    }

    public long getTotalRequests() {
        return totalRequests;
    }

    public void setTotalRequests(final long totalRequests) {
        this.totalRequests = totalRequests;
    }

    public long getSuccessRequests() {
        return successRequests;
    }

    public void setSuccessRequests(final long successRequests) {
        this.successRequests = successRequests;
    }

    public long getFailedAlbumApiCalls() {
        return failedAlbumApiCalls;
    }

    public void setFailedAlbumApiCalls(final long failedAlbumApiCalls) {
        this.failedAlbumApiCalls = failedAlbumApiCalls;
    }

    public long getFailedBookApiCalls() {
        return failedBookApiCalls;
    }

    public void setFailedBookApiCalls(final long failedBookApiCalls) {
        this.failedBookApiCalls = failedBookApiCalls;
    }


    @Override
    public String toString() {
        return "ServiceStatus{" +
                "uptime=" + uptime +
                ", totalRequests=" + totalRequests +
                ", successRequests=" + successRequests +
                ", averageResponseTime=" + averageResponseTime +
                ", averageAlbumApiCallTime=" + averageAlbumApiCallTime +
                ", averageBookApiCallTime=" + averageBookApiCallTime +
                ", failedAlbumApiCalls=" + failedAlbumApiCalls +
                ", failedBookApiCalls=" + failedBookApiCalls +
                '}';
    }

}
