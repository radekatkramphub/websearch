package com.kramphub.websearch.common.exception;

/**
 * @author: nima.abt
 * @since: 9/24/17
 */
public class WebSearchException extends Exception {

    public final int errorCode;

    /**
     * constructor
     *
     * @param errorCode corresponding error-code
     */
    public WebSearchException(final int errorCode){
        super(String.valueOf(errorCode));
        this.errorCode = errorCode;
    }


    /**
     * constructor
     *
     * @param errorCode corresponding error-code
     * @param message   exception message
     */
    public WebSearchException(final int errorCode, final String message){
        super(message);
        this.errorCode = errorCode;
    }

    @Override
    public String toString() {
        return "WebSearchException { errorCode: " + errorCode + ", message: " + super.getMessage() + " }";
    }


}
