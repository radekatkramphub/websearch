package com.kramphub.websearch.common.entity.google;

import java.io.Serializable;

/**
 * @author: nima.abt
 * @since: 9/25/17
 */
public class GoogleBookSearchResult implements Serializable {

    private String id;
    private String kind;
    private String etag;
    private String selfLink;
    private VolumeInfo volumeInfo;

    public GoogleBookSearchResult(){

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public String getSelfLink() {
        return selfLink;
    }

    public void setSelfLink(String selfLink) {
        this.selfLink = selfLink;
    }

    public VolumeInfo getVolumeInfo() {
        return volumeInfo;
    }

    public void setVolumeInfo(VolumeInfo volumeInfo) {
        this.volumeInfo = volumeInfo;
    }


    @Override
    public String toString() {
        return "GoogleBookSearchResult{" +
                "id='" + id + '\'' +
                ", kind='" + kind + '\'' +
                ", etag='" + etag + '\'' +
                ", selfLink='" + selfLink + '\'' +
                ", volumeInfo=" + volumeInfo +
                '}';
    }



}
