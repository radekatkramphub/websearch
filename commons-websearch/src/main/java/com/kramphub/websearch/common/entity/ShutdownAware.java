package com.kramphub.websearch.common.entity;

/**
 * @author: nima.abt
 * @since: 9/24/17
 */
public interface ShutdownAware {

    void onShutdown();

}
