package com.kramphub.websearch.common.dao;

import com.kramphub.websearch.common.entity.google.GoogleBookSearchResponse;
import com.kramphub.websearch.common.exception.WebSearchException;

/**
 * @author: nima.abt
 * @since: 9/24/17
 */
public interface BookSearchDao {

    public GoogleBookSearchResponse search(String keyword, int maxResultCount) throws WebSearchException;


}
