package com.kramphub.websearch.common.entity.response;

/**
 * @author: nima.abt
 * @since: 9/24/17
 */
public class GenericErrorResponse extends AbstractResponse {


    public GenericErrorResponse(){

    }

    public GenericErrorResponse(final int errorCode){
        super(errorCode);
    }


    @Override
    public String toString() {
        return "GenericErrorResponse{" + super.toString() + "}";
    }


}
