package com.kramphub.websearch.common.entity.response;

/**
 * @author: nima.abt
 * @since: 9/25/17
 */
public enum SearchType {

    ALBUM((int) 1),
    BOOK((int) 2);

    private final int value;


    private SearchType(final int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }


    public static SearchType valueOf(final int value) {
        for (final SearchType val : values()) {
            if (val.value == value)
                return val;
        }
        return null;
    }


}
