package com.kramphub.websearch.common.entity;

import com.kramphub.websearch.common.entity.response.SearchType;

import java.io.Serializable;

/**
 * @author: nima.abt
 * @since: 9/25/17
 */
public class SearchResult implements Serializable {


    private String title;
    private SearchType type;
    private String authors;


    public SearchResult(){

    }

    public SearchResult(final String title, final SearchType type, final String authors){
        this.title = title;
        this.type = type;
        this.authors = authors;
    }



    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public SearchType getType() {
        return type;
    }

    public void setType(final SearchType type) {
        this.type = type;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(final String authors) {
        this.authors = authors;
    }


    @Override
    public String toString() {
        return "SearchResult{" +
                "title='" + title + '\'' +
                ", type=" + type +
                ", authors='" + authors + '\'' +
                '}';
    }




}
