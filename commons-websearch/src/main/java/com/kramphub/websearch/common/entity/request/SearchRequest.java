package com.kramphub.websearch.common.entity.request;

import java.io.Serializable;

/**
 * @author: nima.abt
 * @since: 9/24/17
 */
public class SearchRequest implements Serializable {

    private String keyword;


    public SearchRequest(){

    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(final String keyword) {
        this.keyword = keyword;
    }


    @Override
    public String toString() {
        return "SearchRequest{" +
                "keyword='" + keyword + '\'' +
                '}';
    }
}
