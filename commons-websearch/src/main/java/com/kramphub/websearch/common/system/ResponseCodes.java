package com.kramphub.websearch.common.system;

/**
 * @author: nima.abt
 * @since: 9/24/17
 */
public class ResponseCodes {

    public static final int RESP_OK = 0;

    public static final int ERR_EMPTY_KEYWORD = -1001;

    public static final int ERR_UPSTREAM_CALL_FAILED = -2001;

    public static final int ERR_INTERNAL_ERROR = -9001;


}
