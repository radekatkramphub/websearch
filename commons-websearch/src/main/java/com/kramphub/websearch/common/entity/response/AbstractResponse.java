package com.kramphub.websearch.common.entity.response;

import java.io.Serializable;

/**
 * @author: nima.abt
 * @since: 9/24/17
 */
public class AbstractResponse implements Serializable {

    public int status;

    public AbstractResponse(){

    }

    public AbstractResponse(final int status){
        this.status = status;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(final int status) {
        this.status = status;
    }


    @Override
    public String toString() {
        return "status=" + status;
    }


}
