package com.kramphub.websearch.common.entity.response;

import com.kramphub.websearch.common.entity.ServiceStatus;

/**
 * @author: nima.abt
 * @since: 9/27/17
 */
public class ServiceStatusResponse extends AbstractResponse {

    private ServiceStatus serviceStatus;


    public ServiceStatusResponse(){

    }

    public ServiceStatusResponse(final int status, final ServiceStatus serviceStatus){
        super(status);
        this.serviceStatus = serviceStatus;
    }


    public ServiceStatus getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(final ServiceStatus serviceStatus) {
        this.serviceStatus = serviceStatus;
    }


    @Override
    public String toString() {
        return "ServiceStatusResponse{" +
                "serviceStatus=" + serviceStatus +
                '}';
    }


}
