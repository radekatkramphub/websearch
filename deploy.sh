#!/bin/bash

source ./websearch.inc
VERSION="$PROJECT_VERSION"
MODULES="websearch-service"

# === configuration ===
DEST=/tmp/websearch-$VERSION
# === /configuration ===


echo -e "\n\n\n"

echo "----- init: deploy -----"
echo -e "\tdate:           \033[1m `date` \033[0m"
echo -e "\tversion:        \033[1m $VERSION \033[0m"
echo -e "\tdestination:    \033[1m $DEST \033[0m"
echo "-------------------------"

echo -e "\n\n"

echo -n "--- removin' the destination directory (if exists) ... "
if [ -e "$DEST" ]; then
    rm -r "$DEST"
fi
echo -e " \033[1m done. \033[0m"

echo -e "\n"

echo -n "--- create the destination addr ... "
mkdir "$DEST"
cp  websearch.inc $DEST
echo -e " \033[1m done. \033[0m"


echo -e "\n"

for i in $MODULES; do

    echo " --- deployin' module: $i"
    mod_addr="$DEST/$i"
    mod_addr_bin="$mod_addr/bin"
    mod_addr_lib="$mod_addr/lib"

    echo -en "\t --- creating dirs ... "
    mkdir $mod_addr
    mkdir $mod_addr_bin
    mkdir $mod_addr_lib
    echo -e " \033[1m done. \033[0m"

    echo -en "\t --- copyin' jar libs ... "
    cp  $i/target/$i.jar $mod_addr_lib
    cp  $i/target/lib/*jar $mod_addr_lib
    echo -e " \033[1m done. \033[0m"

    echo -en "\t --- copyin' bin scripts ... "
    cp  $i/bin/* $mod_addr_bin
    echo -e " \033[1m done. \033[0m"

    echo -e "\t done with: $i"
done;

echo -e "\n"

echo -e "\033[1m deploy op. completed [project binary dir: $DEST ] :) \033[0m \n\n\n"






