package com.kramphub.websearch.test;

import com.kramphub.websearch.common.dao.AlbumSearchDao;
import com.kramphub.websearch.common.dao.BookSearchDao;
import com.kramphub.websearch.common.entity.response.SearchResponse;
import com.kramphub.websearch.common.exception.WebSearchException;
import com.kramphub.websearch.common.system.ResponseCodes;
import com.kramphub.websearch.common.dao.TestITunesAlbumSearchDao;
import com.kramphub.websearch.common.dao.TestGoogleBookSearchDao;
import com.kramphub.websearch.service.async.AsyncSearchService;
import com.kramphub.websearch.service.async.GoogleBookAsyncSearchService;
import com.kramphub.websearch.service.async.ITunesAlbumAsyncSearchService;
import com.kramphub.websearch.service.impl.WebSearchServicesImpl;
import com.kramphub.websearch.service.system.InMemoryServiceStatusManager;
import com.kramphub.websearch.service.system.ServiceStatusManager;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.assertEquals;

/**
 * @author: nima.abt
 * @since: 9/29/17
 */
public class SearchServiceTest {

    private final AlbumSearchDao albumSearchDao;
    private final BookSearchDao bookSearchDao;
    private final ServiceStatusManager serviceStatusManager;
    private final int albumMaxResultCount = 5;
    private final int bookMaxResultCount = 5;

    public SearchServiceTest() throws Exception {
        albumSearchDao = new TestITunesAlbumSearchDao("/test-itunes-response.txt");
        bookSearchDao = new TestGoogleBookSearchDao("/test-gbook-response.txt");
        serviceStatusManager = new InMemoryServiceStatusManager(10);
    }

    @Test
    public void simpleSearchServiceTest() throws WebSearchException {

        final String keyword = "pink floyd";

        final AsyncSearchService albumAsyncSearchService = new ITunesAlbumAsyncSearchService(albumSearchDao,serviceStatusManager);
        final AsyncSearchService bookAsyncSearchService = new GoogleBookAsyncSearchService(bookSearchDao,serviceStatusManager);


        WebSearchServicesImpl webSearchServices = new WebSearchServicesImpl(albumAsyncSearchService,bookAsyncSearchService,albumMaxResultCount,bookMaxResultCount,serviceStatusManager);

        final SearchResponse searchResponse =  webSearchServices.search(UUID.randomUUID().toString(),keyword);
        assertEquals(searchResponse.getStatus(), ResponseCodes.RESP_OK);
        assertEquals(searchResponse.getResults().length, 10);

    }

}
