package com.kramphub.websearch.common.dao;

import com.google.gson.Gson;
import com.kramphub.websearch.common.entity.itunes.ITunesAlbumSearchResponse;
import com.kramphub.websearch.common.exception.WebSearchException;
import org.apache.commons.io.IOUtils;

/**
 * @author: nima.abt
 * @since: 9/29/17
 */
public class TestITunesAlbumSearchDao implements AlbumSearchDao {


    private final String data;

    public TestITunesAlbumSearchDao(final String resourceAddr) throws Exception {
        data = IOUtils.toString(this.getClass().getResourceAsStream(resourceAddr), "UTF-8");
    }


    public ITunesAlbumSearchResponse search(final String keyword, final int maxResultCount) throws WebSearchException {
        return new Gson().fromJson(data,ITunesAlbumSearchResponse.class);
    }


}
