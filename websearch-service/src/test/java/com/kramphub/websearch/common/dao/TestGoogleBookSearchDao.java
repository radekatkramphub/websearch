package com.kramphub.websearch.common.dao;

import com.google.gson.Gson;
import com.kramphub.websearch.common.entity.google.GoogleBookSearchResponse;
import com.kramphub.websearch.common.exception.WebSearchException;
import org.apache.commons.io.IOUtils;

/**
 * @author: nima.abt
 * @since: 9/29/17
 */
public class TestGoogleBookSearchDao implements BookSearchDao {

    private final String data;

    public TestGoogleBookSearchDao(final String resourceAddr) throws Exception {
        data = IOUtils.toString(this.getClass().getResourceAsStream(resourceAddr), "UTF-8");
    }


    public GoogleBookSearchResponse search(String keyword, int maxResultCount) throws WebSearchException {
        return new Gson().fromJson(data,GoogleBookSearchResponse.class);
    }
}
