package com.kramphub.websearch.service.impl;

import com.kramphub.websearch.common.entity.response.SearchResponse;
import com.kramphub.websearch.common.entity.response.ServiceStatusResponse;
import com.kramphub.websearch.common.exception.WebSearchException;

/**
 * @author: nima.abt
 * @since: 9/24/17
 */
public interface WebSearchServices {


    public SearchResponse search(String requestId, String keyword) throws WebSearchException;

    public ServiceStatusResponse getServiceStatus();



}
