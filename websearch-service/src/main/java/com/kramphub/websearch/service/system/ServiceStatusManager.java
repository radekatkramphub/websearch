package com.kramphub.websearch.service.system;

import com.kramphub.websearch.common.entity.ServiceStatus;
import com.kramphub.websearch.common.entity.response.AbstractResponse;

/**
 * @author: nima.abt
 * @since: 9/29/17
 */
public interface ServiceStatusManager {


    public void incITunesAlbumEndpointFailure();

    public void incGoogleBookEndpointFailureCount();

    public void reportApiRequest(final AbstractResponse response, final long requestTime);

    public ServiceStatus getServiceStatus();

    public void reportAlbumApiCall(long time);

    public void reportBookApiCall(long time);



}
