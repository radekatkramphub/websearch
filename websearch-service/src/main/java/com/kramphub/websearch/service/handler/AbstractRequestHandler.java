package com.kramphub.websearch.service.handler;

import com.google.gson.Gson;
import com.kramphub.websearch.common.entity.response.AbstractResponse;
import org.apache.log4j.Logger;

/**
 * @author: nima.abt
 * @since: 9/24/17
 */
public abstract class AbstractRequestHandler {



    public <T extends AbstractResponse> String getResponse(final String reqId, final T obj){

        try {
            return new Gson().toJson(obj);
        } catch (Exception e) {
            getLogger().error("{reqId: " + reqId + "} failed to encrypt response!");
            return null;
        }

    }


    protected abstract Logger getLogger();

}
