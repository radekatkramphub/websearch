package com.kramphub.websearch.service.system;

import com.kramphub.websearch.common.entity.ServiceStatus;
import com.kramphub.websearch.common.entity.response.AbstractResponse;
import com.kramphub.websearch.common.system.ResponseCodes;
import org.apache.commons.collections.buffer.CircularFifoBuffer;
import org.apache.log4j.Logger;

import java.util.Iterator;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author: nima.abt
 * @since: 9/27/17
 */
// todo : better to use redis or memcached, this impl. has been chosen to reduce the project's requirements
public class InMemoryServiceStatusManager implements ServiceStatusManager {

    private final long startTime;

    private final AtomicLong totalRequestsCounter = new AtomicLong(0);
    private final AtomicLong successResponseCounter = new AtomicLong(0);

    private final AtomicLong itunesAlbumEndpointFailureCounter = new AtomicLong(0);
    private final AtomicLong googleBookEndpointFailureCounter = new AtomicLong(0);


    private final CircularFifoBuffer latestResponseTimes;
    private final CircularFifoBuffer albumApiCallResponseTimes;
    private final CircularFifoBuffer bookApiCallResponseTimes;

    private final Logger logger = Logger.getLogger(InMemoryServiceStatusManager.class);

    public InMemoryServiceStatusManager(final int responseAverageTimeWindowSize) {
        logger.info("init: InMemoryServiceStatusManager ... ");
        this.startTime = System.currentTimeMillis();
        latestResponseTimes = new CircularFifoBuffer(responseAverageTimeWindowSize);
        albumApiCallResponseTimes = new CircularFifoBuffer(responseAverageTimeWindowSize);
        bookApiCallResponseTimes = new CircularFifoBuffer(responseAverageTimeWindowSize);
    }


    public void incITunesAlbumEndpointFailure() {
        itunesAlbumEndpointFailureCounter.incrementAndGet();
    }

    public void incGoogleBookEndpointFailureCount() {
        googleBookEndpointFailureCounter.incrementAndGet();
    }

    public void reportApiRequest(final AbstractResponse response, final long requestTime) {
        totalRequestsCounter.incrementAndGet();
        latestResponseTimes.add(System.currentTimeMillis() - requestTime);
        if (response.getStatus() == ResponseCodes.RESP_OK) {
            successResponseCounter.incrementAndGet();
        }
    }


    public void reportAlbumApiCall(final long time) {
        albumApiCallResponseTimes.add(time);
    }

    public void reportBookApiCall(long time) {
        bookApiCallResponseTimes.add(time);
    }

    public ServiceStatus getServiceStatus() {


        return new ServiceStatus(
                System.currentTimeMillis() - startTime,
                totalRequestsCounter.get(),
                successResponseCounter.get(),
                calculateAverage(latestResponseTimes),
                calculateAverage(albumApiCallResponseTimes),
                calculateAverage(bookApiCallResponseTimes),
                itunesAlbumEndpointFailureCounter.get(),
                googleBookEndpointFailureCounter.get()
        );


    }



    private long calculateAverage(CircularFifoBuffer buffer) {

        int count = 0, sum = 0;
        synchronized (buffer) {
            final Iterator<Long> iterator = buffer.iterator();
            while (iterator.hasNext()) {
                count++;
                sum += iterator.next();
            }
        }
        return count > 0 ? (sum / count) : 0;

    }







}
