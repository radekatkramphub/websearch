package com.kramphub.websearch.service.handler;

import com.github.nimabt.renetty.http.annotation.*;
import com.github.nimabt.renetty.http.model.RequestMethod;
import com.github.nimabt.renetty.http.netty.HttpRequestHandler;
import com.kramphub.websearch.common.entity.response.GenericErrorResponse;
import com.kramphub.websearch.common.entity.response.SearchResponse;
import com.kramphub.websearch.common.entity.response.ServiceStatusResponse;
import com.kramphub.websearch.common.exception.WebSearchException;
import com.kramphub.websearch.service.impl.WebSearchServices;
import com.kramphub.websearch.service.system.ServiceStatusManager;
import org.apache.log4j.Logger;

/**
 * @author: nima.abt
 * @since: 9/24/17
 */
public class WebSearchRequestHandler extends AbstractRequestHandler implements HttpRequestHandler {

    private final WebSearchServices webSearchServices;
    private final ServiceStatusManager serviceStatusManager;

    private final Logger logger = Logger.getLogger(WebSearchRequestHandler.class);


    public WebSearchRequestHandler(final WebSearchServices webSearchServices, final ServiceStatusManager serviceStatusManager) {
        this.webSearchServices = webSearchServices;
        this.serviceStatusManager = serviceStatusManager;
    }




    @PreIntercept(method = RequestMethod.GET)
    public void preIntercept(
            final @RequestId String requestId,
            final @RequestPath String requestPath,
            final @QueryParam(key = "keyword") String keyword,
            final @IpAddress String ipAddress
    ) {
        logger.info("{" + requestId + "} got request (GET): " + requestPath + " , keyword: " + keyword + " , from: " + ipAddress);
    }


    @PostIntercept(method = RequestMethod.GET)
    public void postIntercept(
            final @RequestId String requestId,
            final @ResponseStatus int responseStatus,
            final @ResponseBody String responseBody,
            final @RequestTime long startTime
    ) {
        logger.info("{" + requestId + "} returned response (" + responseStatus + ") , data: " + responseBody + ", took: " + (System.currentTimeMillis() - startTime) + " msecs");
    }





    @HttpRequest(path = "/api/search", method = RequestMethod.GET, responseContentType = "application/json")
    public String search(final @RequestId String requestId, final @QueryParam(key = "keyword") String keyword, final @RequestTime long startTime) {

        try {
            final SearchResponse response = webSearchServices.search(requestId, keyword);
            serviceStatusManager.reportApiRequest(response,startTime);
            return getResponse(requestId, response);
        } catch (WebSearchException e) {
            logger.warn("{" + requestId + "} WebSearchException occurred while processing request; reason: " + e.getMessage());
            final GenericErrorResponse errorResponse = new GenericErrorResponse(e.errorCode);
            serviceStatusManager.reportApiRequest(errorResponse,startTime);
            return getResponse(requestId,errorResponse);
        }

    }


    @HttpRequest(path = "/api/service-status", method = RequestMethod.GET, responseContentType = "application/json")
    public String serviceStatus(final @RequestId String requestId) {

        final ServiceStatusResponse response = webSearchServices.getServiceStatus();
        return getResponse(requestId, response);

    }


    protected Logger getLogger() {
        return logger;
    }


}
