package com.kramphub.websearch.service.async;

import com.kramphub.websearch.common.entity.SearchResult;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * @author: nima.abt
 * @since: 9/26/17
 */
public interface AsyncSearchService {


    CompletableFuture<List<SearchResult>> search(String keyword, int maxResultCount);


}
