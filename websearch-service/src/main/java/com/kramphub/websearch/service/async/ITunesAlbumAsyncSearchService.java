package com.kramphub.websearch.service.async;

import com.kramphub.websearch.common.dao.AlbumSearchDao;
import com.kramphub.websearch.common.entity.itunes.ITunesAlbumSearchResponse;
import com.kramphub.websearch.common.entity.SearchResult;
import com.kramphub.websearch.common.exception.WebSearchException;
import com.kramphub.websearch.common.util.SearchUtil;
import com.kramphub.websearch.service.system.ServiceStatusManager;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * @author: nima.abt
 * @since: 9/26/17
 */
public class ITunesAlbumAsyncSearchService implements AsyncSearchService {

    private final AlbumSearchDao dao;
    private final ServiceStatusManager serviceStatusManager;

    private final Logger logger = Logger.getLogger(ITunesAlbumAsyncSearchService.class);

    public ITunesAlbumAsyncSearchService(final AlbumSearchDao dao, final ServiceStatusManager serviceStatusManager){
        this.dao = dao;
        this.serviceStatusManager = serviceStatusManager;

    }

    @Override
    public CompletableFuture<List<SearchResult>> search(String keyword, int maxResultCount) {
        return CompletableFuture.supplyAsync(() -> getResponse(keyword,maxResultCount));
    }


    private List<SearchResult> getResponse(final String keyword, final int maxResultCount){

        final long start = System.currentTimeMillis();
        List<SearchResult> result = new LinkedList<>();
        try{
            final ITunesAlbumSearchResponse iTunesAlbumSearchResponse = dao.search(keyword,maxResultCount);
            result = SearchUtil.toSearchResult(iTunesAlbumSearchResponse);
        } catch (WebSearchException e){
            logger.error("WebSearchException occurred while trying to search for: " + keyword + "; reason: " + e.getMessage());
            serviceStatusManager.incITunesAlbumEndpointFailure();
        }
        serviceStatusManager.reportAlbumApiCall(System.currentTimeMillis()-start);
        return result;

    }

}
