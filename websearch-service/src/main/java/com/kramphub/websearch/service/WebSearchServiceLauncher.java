package com.kramphub.websearch.service;

import com.kramphub.websearch.common.entity.ShutdownAware;
import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Map;

/**
 * @author: nima.abt
 * @since: 9/24/17
 */
public class WebSearchServiceLauncher {


    private static final Logger logger = Logger.getLogger(WebSearchServiceLauncher.class);

    public static void main(String[] args) {
        logger.info("init: WebSearchServiceLauncher ... ");

        logger.debug("loading applicationContext ...");
        final ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
                new String[]{"spring/context-websearch-service.xml"}
        );

        logger.info("websearch-service: up-n-running ... :) ");


        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                logger.info("shutdown signal triggered ! (prepare to stop the process gracefully) ...");
                @SuppressWarnings({"unchecked"})
                final Map<?, ShutdownAware> shutdownBeansMap = context.getBeansOfType(ShutdownAware.class);
                logger.info("shutting down #" + shutdownBeansMap.size() + " shutdown-aware beans ...");
                for (final ShutdownAware sa : shutdownBeansMap.values()) {
                    sa.onShutdown();
                }
            }
        });

    }



}
