package com.kramphub.websearch.service.impl;

import com.kramphub.websearch.common.entity.ServiceStatus;
import com.kramphub.websearch.common.entity.response.ServiceStatusResponse;
import com.kramphub.websearch.service.async.AsyncSearchService;
import com.kramphub.websearch.common.entity.response.SearchResponse;
import com.kramphub.websearch.common.entity.SearchResult;
import com.kramphub.websearch.common.exception.WebSearchException;
import com.kramphub.websearch.common.system.ResponseCodes;
import com.kramphub.websearch.service.system.ServiceStatusManager;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author: nima.abt
 * @since: 9/24/17
 */
public class WebSearchServicesImpl implements WebSearchServices {


    private final int albumMaxResultCount;
    private final int bookMaxResultCount;

    private final AsyncSearchService albumAsyncSearchService;
    private final AsyncSearchService bookAsyncSearchService;

    private final Comparator<SearchResult> searchResultComparator;

    private final ServiceStatusManager serviceStatusManager;

    private final Logger logger = Logger.getLogger(WebSearchServicesImpl.class);

    public WebSearchServicesImpl(
            final AsyncSearchService albumAsyncSearchService,
            final AsyncSearchService bookAsyncSearchService,
            final int albumMaxResultCount,
            final int bookMaxResultCount,
            final ServiceStatusManager serviceStatusManager
    ) {
        logger.info("init: WebSearchServicesImpl ... ");
        this.albumMaxResultCount = albumMaxResultCount;
        this.bookMaxResultCount = bookMaxResultCount;

        this.albumAsyncSearchService = albumAsyncSearchService;
        this.bookAsyncSearchService = bookAsyncSearchService;

        final Comparator<String> nullSafeStringComparator = Comparator.nullsLast(String::compareToIgnoreCase);
        searchResultComparator = Comparator.comparing(SearchResult::getTitle, nullSafeStringComparator);

        this.serviceStatusManager = serviceStatusManager;

    }


    public SearchResponse search(final String requestId, final String keyword) throws WebSearchException {

        logger.info("{reqId: " + requestId + "} prepare to search: " + keyword);

        if (StringUtils.isBlank(keyword)) {
            throw new WebSearchException(ResponseCodes.ERR_EMPTY_KEYWORD, "{reqId: " + requestId + "} empty keyword !");
        }

        final CompletableFuture<List<SearchResult>> albums = albumAsyncSearchService.search(keyword,albumMaxResultCount);
        final CompletableFuture<List<SearchResult>> books = bookAsyncSearchService.search(keyword,bookMaxResultCount);

        final CompletableFuture<List<SearchResult>> combinedFuture = albums.thenCombine(books, (a, b) ->  Stream.concat(a.stream(), b.stream()).collect(Collectors.toList()));
        final List<SearchResult> result = combinedFuture.join();
        result.sort(searchResultComparator);

        // todo: better to use a key/value nosql cache here (like memcached, or redis) & put the result in cache for a specific time (for further use)

        return new SearchResponse(ResponseCodes.RESP_OK, result.toArray(new SearchResult[result.size()]));

    }



    public ServiceStatusResponse getServiceStatus() {

        final ServiceStatus serviceStatus = serviceStatusManager.getServiceStatus();
        return new ServiceStatusResponse(ResponseCodes.RESP_OK,serviceStatus);

    }




}
